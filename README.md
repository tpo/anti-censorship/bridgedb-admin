BridgeDB Administration
-----------------------

This document describes the repository layout and how it is used in
production.

## Directory layout

- `bin/`

  Contains scripts that are needed to operate BridgeDB in production.

- `doc/`

  Documentations that applies to this repository, not BridgeDB itself.

- `etc/`

  Configuration files that BridgeDB needs. They contain what our production
  setup is using. Any changes to these files should be applied to our
  production deployment.

## Deployment

This section details how BridgeDB is deployed in production on Tor Project
infrastructure and how the files within this repository are used.

### Setup

BridgeDB runs on `polyanthum.torproject.org`. It is run with the user
`bridgedb` so once logged in, one can access the user with:

`$ sudo -u bridgedb bash`

The running directory of BridgeDB is in:

`/srv/bridges.torproject.org`

The `/home/bridgedb` actually points to `/srv/bridges.torproject.org/home` so
everything to run BridgeDB is located within this path.

#### Environement

The production environement paths and files are configured in the following
file. It currently has the production values. This file is sourced before
running any scripts within `bin/`.

`etc/environment-production`

#### Cron

The `etc/bridgedb.crontab` contains the scheduled operations run by the cron
daemon. You can modify this by running:

`$ crontab -e`

#### SSH Keys

Twice per hour, at minute 7 and 37, our Bridge Authority (currently `Serge`)
pushes the descriptors. BridgeDB restricts the authority to one single script:

`bin/store-bridge-tarball-from-serge`

Interestingly enough, Serge will `cat` through the SSH connection the
descriptors and then the above script will `cat > $t` and then put into the
`from-${AUTHORITY_NAME}` directory.

That directory needs to be configured in `etc/bridgedb.conf` using the option
`BRIDGE_AUTHORITY_DIRECTORIES=`.

The SSH configuration is located in `/etc/ssh/userkeys/bridgedb` which is
taken from:

`etc/ssh-userkey`

#### Web Server

BridgeDB runs its own Web server but also has an Apache that acts as a proxy.
The configuration is found in this file which is copied into the apache
configuration directory (`/etc/apache2`):

#### Procmail config

The file `etc/procmailrc` should be linked to the bridgedb user's
`~/.procmailrc`.  The tool `dkimverify`, which is used in the file, takes care
of DKIM verification for us.

`etc/apache.conf`

### Running

The crontab `@reboot bin/run-bridgedb` is used to start it on boot. We'll move
to a systemd init script so it can also handle the restart in case BridgeDB
crashes.
